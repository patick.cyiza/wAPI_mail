source 'https://rubygems.org'
git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end
ruby '2.4.1'
gem 'rails', '~> 5.2.1'
gem 'puma', '~> 3.7'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2'
gem 'turbolinks', '~> 5'
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
gem 'bootstrap', '~> 4.0.0'
gem 'devise'
gem 'haml-rails'
gem 'high_voltage'
gem 'jquery-rails'
gem 'pg', '~> 0.18'
gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
gem "paperclip", "~> 6.0.0"
gem 'access-granted', '~> 1.1.0'
gem 'simple_form'
gem 'i18n', '~> 1.1.0'
gem 'bootsnap', require: false
gem "font-awesome-rails"
gem 'activeadmin'
gem 'attr_encrypted'
gem 'sidekiq'
gem 'will_paginate', '~> 3.1.0'

#mail editor
gem 'webpacker', '~> 4.0.7'
gem 'codemirror-rails'
gem 'letter_opener'
gem 'capybara-email'
#end mail editor

#api gems
gem 'jbuilder', '~> 2.5'
gem 'rack-cors', :require => 'rack/cors'
gem 'json_api_responders'
gem 'jwt', '~> 2.0'
#end api gems

#gems for the emails
gem 'inky-rb', require: 'inky'
gem 'premailer-rails'
gem 'mail_form' #to configure
gem 'roadie', '~> 3.5'
#end emails

#Service providers gems
gem 'mailjet', git: 'https://github.com/mailjet/mailjet-gem.git' #to configure
gem 'mandrill-api'
gem 'sendgrid-ruby'
gem 'mailgun-ruby', '~> 1.1.6'
#end service providers

group :development, :test do
  gem 'capybara', '~> 2.13'
  gem 'selenium-webdriver'
  gem 'factory_bot_rails'
  gem 'faker'
  gem 'rspec-rails'
end

group :development do
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'better_errors'
  gem 'guard-bundler'
  gem 'guard-rails'
  gem 'guard-rspec'
  gem 'html2haml'
  gem 'rails_layout'
  gem 'rb-fchange', :require=>false
  gem 'rb-fsevent', :require=>false
  gem 'rb-inotify', :require=>false
  gem 'spring-commands-rspec'
  gem 'pry-byebug'
end

group :test do
  gem 'database_cleaner'
  gem 'launchy'
end

group :production do
  gem 'aws-sdk-s3'
  gem 'exception_notification'
end
