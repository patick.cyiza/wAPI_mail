ActiveAdmin.register SendingProviderKey do

  permit_params :sending_provider_id, :key_name

end
