ActiveAdmin.register SendingProvider do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
  permit_params :name, :sending_controller_url, :api_access_docs_url, :build_state
  form do |f|
    build_states = SendingProvider.enum_build_states

    f.semantic_errors # shows errors on :base
    f.inputs do
      f.input :name
      f.input :sending_controller_url
      f.input :api_access_docs_url
      f.input :build_state, as: :select, collection: build_states, selected: f.sending_provider.build_state || build_states.first
    end
    f.actions         # adds the 'Submit' and 'Cancel' buttons
  end

end
