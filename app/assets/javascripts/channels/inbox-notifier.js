function initInboxSub (project_id) {
    App["inbox_notifier" + project_id] = App.cable.subscriptions.create(
        {
            channel: "InboxNotifierChannel",
            project_id
        },
        {
            connected: () => {
                console.log("connected to inbox-" + project_id)
            },
            disconnected: () => {
                console.log("disconnected")
            },
            received: data => {
                console.log("recived")
                $('#messages-'+ project_id).append(data['message'])
            }
        })

    const UnsubHandeler = () => {
        App["inbox_notifier" + project_id].unsubscribe()
        console.log("unsubscribed")
        document.removeEventListener("turbolinks:before-render", UnsubHandeler, false)
    }

    document.addEventListener("turbolinks:before-render", UnsubHandeler, false)
}