class InboxNotifierChannel < ApplicationCable::Channel
  def subscribed
    if project_id = params[:project_id]
      if current_user.is_project_id_owner? project_id
        stream_from"inbox-#{project_id}"
      else
        connection.transmit identifier: params, error: "Project_id #{params[:project_id]} not found or Unauthorized."
        reject
      end
      # creates a private chat room with a unique name
    end
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end
end
