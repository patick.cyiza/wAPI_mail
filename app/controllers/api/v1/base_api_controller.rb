class Api::V1::BaseApiController < ActionController::API
  I18n.locale = :en
  include JsonApiResponders
  respond_to :json

end
