class Api::V1::TransmitApiController < Api::V1::BaseApiController
  before_action :authorize_and_set_payload, only: :transmit_transaction

  def transmit_transaction
    if @transaction = @key.project.transactions.find_by(code_name: params[:code_name])
      begin
        @mail_content = @transaction.parse_transaction @transaction_params[:vars]
        @correspondents = {from: @transaction.mail_from, to: @transaction_params[:send_to]}
        self.send "transmit_#{@key.env_name}"
      rescue => e
        render json: { message: 'One or more required variables not given. Check the your payload',  status: 400 }, status: 400
        logger.error e.message
        e.backtrace.take(3).each { |line| logger.error line }
      end

    else
      render json: { message: 'Transaction not found, is the given code name of the transaction correct?',  status: 404 }, status: 404
    end
  end

  private

  def transmit_DEVELOPMENT
    begin
      BroadcastToDevInboxJob.perform_later(@key.project.id, @transaction.id, @mail_content, @correspondents)
      on_broadcast_success
    rescue => e
      on_broadcast_failure e
    end
  end

  def transmit_PRODUCTION
    @credentials = @transaction.project.provider_credentials
    sp_name = @credentials["provider"]

    if Rails.env.production? && SendingProvider.where(build_state: "supported").exists?(name: sp_name)
      dispatch_to_provider_worker
    elsif Rails.env.development? && SendingProvider.exists?(name: sp_name)
      dispatch_to_provider_worker
    end
  end

  def dispatch_to_provider_worker
    begin
      sp_name = @credentials["provider"]
      "dispatch/#{sp_name}_job".classify.constantize.perform_later(@credentials, @mail_content, @correspondents)
      on_dispatch_success
    rescue => e
      on_dispatch_failure e
    end
  end

  def on_dispatch_success
    render json: {
        env: "PRODUCTION",
        provider: @credentials["provider"],
        transaction: @transaction.code_name,
        send_to: @transaction_params[:send_to]
    }, status: :ok
  end

  def on_dispatch_failure e
    render json: { message: "Internal server error", status: 500 }, status: 500
    logger.error e.message
  end

  def on_broadcast_success
    render json: {
        env: "DEVELOPMENT",
        transaction: @transaction.code_name,
        send_to: @transaction_params[:send_to]
    }, status: :ok
  end

  def on_broadcast_failure e
    render json: {
        message: "Internal server error while broacasting to the dev_inboxes of #{@key.project.name}",
        status: 500
    }, status: 500
    logger.error e.message
  end

  def deliver_params
    params.require(:transaction).permit(:send_to, :vars => {})
  end

  def authorize_and_set_payload
    begin
      token = request.headers['Authorization'].gsub(/^Bearer /, '')
      if signed_token = JwtService.get_authorized_payload(token)
        @key = signed_token[:key]
        @transaction_params = signed_token[:transaction_params]
      else
        render json: { message: 'Not authorized',  status: 401}, status: 401
      end
    rescue
      render json: { message: 'No JWT token given in the authorization header of the request.',  status: 401}, status: 401
    end
  end

end
