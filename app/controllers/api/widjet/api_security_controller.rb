class Api::Widjet::ApiSecurityController < Api::Widjet::BaseController
  before_action :set_key, only: [:change_locks]

  def generate_key
    begin
      case params[:env]
      when "DEVELOPMENT"
        render json: @project.generate_send_api_dev_key, status: :ok
      when "PRODUCTION"
        render json: @project.generate_send_api_prod_key, status: :ok
      else
        render json: {message: "Error, no environment given in the request"}, status: :unprocessable_entity
      end
    rescue
      render json: {message: "Error, could not create api key"}, status: :unprocessable_entity
    end
  end

  def change_locks
    if respond = @key.change_locks
      render json: respond, status: :ok
    else
      render json: {message: "Error, could not change api key"}, status: :unprocessable_entity
    end
  end

  private
  def set_key
    if @key = ProjectApiKey.find(params[:id])
    else
      render json: {message: "no key instance found"}, status: :unprocessable_entity
    end
  end

end