class Api::Widjet::BaseController < ApplicationController
  before_action :set_project

  include Api::V1::BaseApiHelper


  private
  # Use callbacks to share common setup or constraints between actions.
  def set_project
    @project = Project.find(params[:project_id])
    authorize! :crud, @project
  end
end
