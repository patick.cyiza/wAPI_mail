class Api::Widjet::DesignerController < Api::Widjet::BaseController
  before_action :set_designable, only: [:update_designable, :get_external_assets, :create_local_picture_asset]

  def preview
    zipped_params = JwtService.params_zipper(preview_params.to_h)
    previewer_path = URI.join(root_url, "/rails/mailers/transmit_api/check_result?zipped_params=#{zipped_params}")
    respond_to do |format|
      format.html { redirect_to previewer_path }
      format.json { render json: {result_path: previewer_path}, status: :ok }
    end
  end

  def update_designable
    if @designable.update(designable_params)
      render json: @designable, status: :ok
    else
      render json: {message: "in #{@designable.errors.first[0]}, #{@designable.errors.first[1]}"}, status: :unprocessable_entity
    end
  end

  def create_local_picture_asset
    @asset = @designable.sendable_assets.build(picture_asset_params)
    create_image_asset(@asset)
  end

  def create_global_picture_asset
    @asset = @project.sendable_assets.build(picture_asset_params)
    create_image_asset(@asset)
  end

  def destroy_asset
    sendable_asset = SendableAsset.find(params[:sendable_asset_id])
    sendable_asset.destroy
    head :no_content
  end

  private
  def set_designable
    if @designable = DesignableWrapper.get_designable(params[:designable_type], params[:id])
    else
      render json: {message: "no designable instance found"}, status: :unprocessable_entity
    end
  end

  def create_image_asset imported_asset
    imported_asset.asset_type = "Picture"
    imported_asset.code_name = generate_code_name(picture_asset_params[:code_name], imported_asset.predicted_id)

    if imported_asset.save
      render json: {id: imported_asset.id, code_name: imported_asset.code_name, thumb: imported_asset.get_image(:tiny), img: imported_asset.get_image}
    else
      render json: {message: "in #{imported_asset.errors.first[0]}, #{imported_asset.errors.first[1]}"}, status: :unprocessable_entity
    end
  end

  def preview_params
    params.require(:previewer).permit(:from, :subject, :content_img, :styling_img)
  end

  def designable_params
    params.require(:designable).permit(:content_img, :styling_img, :subject)
  end

  def picture_asset_params
    params.require(:sendable_asset).permit( :code_name, picture_attributes: :illustration)
  end

  def external_asset_params
    params.require(:transaction).permit(
        external_assets_attributes: [:id, :url, :_destroy]
    )
  end

end