class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

  rescue_from "AccessGranted::AccessDenied" do |exception|
    redirect_back fallback_location: root_path, alert: "You don't have permission to access this page."
  end

  def home
    if current_user
      redirect_to projects_path
    else
      redirect_to '/users/sign_in'
    end
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
    devise_parameter_sanitizer.permit(:account_update, keys: [:name])
  end

end
