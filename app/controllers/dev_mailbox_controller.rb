class DevMailboxController < ApplicationController
  before_action :authenticate_user!
  layout "dev_mailbox"

  def index
    @projects = current_user.projects.joins(:project_api_keys).distinct.order(:created_at)
  end

end
