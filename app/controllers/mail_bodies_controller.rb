class MailBodiesController < ApplicationController
  before_action :set_mail_body, only: [:show, :edit, :update, :destroy]

  # GET /mail_bodies
  # GET /mail_bodies.json
  def index
    @mail_bodies = MailBody.all
  end

  # GET /mail_bodies/1
  # GET /mail_bodies/1.json
  def show
  end

  # GET /mail_bodies/new
  def new
    @mail_body = MailBody.new
  end

  # GET /mail_bodies/1/edit
  def edit
  end

  # POST /mail_bodies
  # POST /mail_bodies.json
  def create
    @mail_body = MailBody.new(mail_body_params)

    respond_to do |format|
      if @mail_body.save
        format.html { redirect_to @mail_body, notice: 'Mail body was successfully created.' }
        format.json { render :show, status: :created, location: @mail_body }
      else
        format.html { render :new }
        format.json { render json: @mail_body.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /mail_bodies/1
  # PATCH/PUT /mail_bodies/1.json
  def update
    respond_to do |format|
      if @mail_body.update(mail_body_params)
        format.html { redirect_to @mail_body, notice: 'Mail body was successfully updated.' }
        format.json { render :show, status: :ok, location: @mail_body }
      else
        format.html { render :edit }
        format.json { render json: @mail_body.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /mail_bodies/1
  # DELETE /mail_bodies/1.json
  def destroy
    @mail_body.destroy
    respond_to do |format|
      format.html { redirect_to mail_bodies_url, notice: 'Mail body was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_mail_body
      @mail_body = MailBody.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def mail_body_params
      params.require(:mail_body).permit(:user_id, :name, :content_img, :styling_img)
    end
end
