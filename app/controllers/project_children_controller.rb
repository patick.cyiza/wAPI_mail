class ProjectChildrenController < ApplicationController
  before_action :authenticate_user!
  before_action :set_project

  private

  def set_project
    @project = Project.find(params[:project_id])
    authorize! :crud, @project
  end
end