class PublicTemplatesController < ApplicationController
  before_action :set_public_template, only: [:show, :edit, :update, :destroy]

  # GET /public_templates
  # GET /public_templates.json
  def index
    @public_templates = PublicTemplate.all
  end

  # GET /public_templates/1
  # GET /public_templates/1.json
  def show
  end

  # GET /public_templates/new
  def new
    @public_template = PublicTemplate.new
  end

  # GET /public_templates/1/edit
  def edit
  end

  # POST /public_templates
  # POST /public_templates.json
  def create
    @public_template = PublicTemplate.new(public_template_params)

    respond_to do |format|
      if @public_template.save
        format.html { redirect_to @public_template, notice: 'Public template was successfully created.' }
        format.json { render :show, status: :created, location: @public_template }
      else
        format.html { render :new }
        format.json { render json: @public_template.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /public_templates/1
  # PATCH/PUT /public_templates/1.json
  def update
    respond_to do |format|
      if @public_template.update(public_template_params)
        format.html { redirect_to @public_template, notice: 'Public template was successfully updated.' }
        format.json { render :show, status: :ok, location: @public_template }
      else
        format.html { render :edit }
        format.json { render json: @public_template.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /public_templates/1
  # DELETE /public_templates/1.json
  def destroy
    @public_template.destroy
    respond_to do |format|
      format.html { redirect_to public_templates_url, notice: 'Public template was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_public_template
      @public_template = PublicTemplate.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def public_template_params
      params.require(:public_template).permit(:content_img, :styling_img)
    end
end
