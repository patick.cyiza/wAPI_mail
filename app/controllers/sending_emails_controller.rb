class SendingEmailsController < ApplicationController
  before_action :set_project, except: [:set_sending_email, :confirm_email ]
  before_action :set_sending_email, except: [:new, :create, :index, :set_project, :sending_email_params, :confirm_email]
  layout "projects"
  # GET /sending_emails
  # GET /sending_emails.json
  def index
    @sending_emails = @project.sending_emails.includes(:defaulties).order('defaulties.defaultable_type')
  end

  # GET /sending_emails/1
  # GET /sending_emails/1.json
  def show
  end

  # GET /sending_emails/new
  def new
    @sending_email = SendingEmail.new
  end

  # GET /sending_emails/1/edit
  def edit
  end

  # POST /sending_emails
  # POST /sending_emails.json
  def create
    @sending_email = @project.sending_emails.build(sending_email_params)

    respond_to do |format|
      if @sending_email.save
        if sending_email_params[:set_default] == "1"
          @sending_email.update_or_create_default
        end
        send_confirmation_token @sending_email
        format.html { redirect_to project_sending_emails_path(@project), notice: 'Sending email was successfully created, please confirm this email to be able to use it.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /sending_emails/1
  # PATCH/PUT /sending_emails/1.json
  def update
    respond_to do |format|
      old_mail = @sending_email.email
      msg = "Sending email was successfully updated."

      if @sending_email.update(sending_email_params)
        if sending_email_params[:set_default] == "1" && @project.has_as_default?(@sending_email) == false
          @sending_email.update_or_create_default
          msg = "Sending email was successfully set as default."
        end
        # if the email field has been updated resend a confirmation token
        if old_mail != @sending_email.email
          send_confirmation_token
          msg = "Sending email updated, please confirm this email to be able to use it. An email has been sent to #{@sending_email.email} to complete this process."
          format.html { redirect_to project_sending_emails_path(@project), notice: msg }
        else
          format.html { redirect_to project_sending_emails_path(@project), notice: msg }
        end
      else
        format.html { render :edit, msg = "Oops... Somethings went wrong. Please try again." }
      end
    end
  end

  def resend_confirmation
    if @sending_email.is_confirmed? == false
      send_confirmation_token
      respond_to do |format|
        format.html { redirect_to project_sending_emails_path(@project), notice: "An email has been sent to #{@sending_email.email} to complete this process." }
      end
    end
  end

  def confirm_email
    respond_to do |format|
      @project = Project.find(params[:project_id])
      @sending_email = SendingEmail.confirm_by_token(params[:token])
      if @sending_email

        msg = "#{@sending_email.email} was successfully confirmed."
        if current_user
          format.html { redirect_to project_sending_emails_path(@project), notice: msg }
        else
          format.html { redirect_to root_path(@project), notice: msg }
        end
      else

        msg = "Oops... Confirmation went wrong. Request a new confirmation email"
        if current_user
          format.html { redirect_to project_sending_emails_path(@project), alert: msg }
        else
          format.html { redirect_to root_path(@project), alert: msg }
        end
      end
    end
  end

  # DELETE /sending_emails/1
  # DELETE /sending_emails/1.json
  def destroy
    @sending_email.destroy
    respond_to do |format|
      format.html { redirect_to project_sending_emails_path(@project), notice: 'Sending email was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  def set_sending_email
    @sending_email = SendingEmail.find(params[:id])
  end

  def set_project
    @project = Project.find(params[:project_id])
    authorize! :crud, @project
  end

  def send_confirmation_token(sending_email = @sending_email)
    @token = sending_email.create_confirmation_token
    SendingEmailConfirmationMailer.send_confirmation(@project, sending_email, @token).deliver_later
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def sending_email_params
    params.require(:sending_email).permit(
        :project_id, :email, :set_default
    )
  end
end
