class SendingProvidersProjectsController < ProjectChildrenController
  before_action :set_sending_providers_project, only: [:show, :edit, :update, :destroy]
  layout "projects"
  # GET /sending_providers_projects
  # GET /sending_providers_projects.json
  def index
    @sending_providers_projects = @project.sending_providers_projects
    @unused_services = @project.unused_smtp_providers
  end

  # GET /sending_providers_projects/1
  # GET /sending_providers_projects/1.json
  def show
  end

  # GET /sending_providers_projects/new
  def new
    @sending_providers_project = SendingProvidersProject.new(sending_providers_project_params)
  end

  def create_from_provider
    @sending_providers_project = @project.sending_providers_projects.build({sending_provider_id: params[:sending_provider_id]})

    respond_to do |format|
      if @sending_providers_project.save
        format.html { redirect_to edit_project_sending_providers_project_path(@project, @sending_providers_project), notice: 'Sending services project was successfully created.' }
        format.json { render :show, status: :created, location: @sending_providers_project }
      else
        format.html { redirect_to project_sending_providers_projects_path(@project), warn: 'not created' }
        format.json { render json: @sending_providers_project.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /sending_providers_projects/1/edit
  def edit
  end

  # POST /sending_providers_projects
  # POST /sending_providers_projects.json
  def create
    @sending_providers_project = SendingProvidersProject.new(sending_providers_project_params)

    respond_to do |format|
      if @sending_providers_project.save
        format.html { redirect_to project_sending_providers_projects_path(@project), notice: 'Sending services project was successfully created.' }
        format.json { render :show, status: :created, location: @sending_providers_project }
      else
        format.html { render :new }
        format.json { render json: @sending_providers_project.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sending_providers_projects/1
  # PATCH/PUT /sending_providers_projects/1.json
  def update
    respond_to do |format|
      if @sending_providers_project.update(sending_providers_project_params)
        if sending_providers_project_params[:set_default] == "1"
          @sending_providers_project.update_or_create_default
        end
        format.html { redirect_to project_sending_providers_projects_path(@project), notice: 'Sending services project was successfully updated.' }
        format.json { render :show, status: :ok, location: @sending_providers_project }
      else
        format.html { render :edit }
        format.json { render json: @sending_providers_project.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sending_providers_projects/1
  # DELETE /sending_providers_projects/1.json
  def destroy
    @sending_providers_project.destroy
    respond_to do |format|
      format.html { redirect_to project_sending_providers_projects_path(@project), notice: 'Sending services project was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_sending_providers_project
    @sending_providers_project = SendingProvidersProject.find(params[:id])
    @provider = @sending_providers_project.sending_provider
    @provider_key = @provider.sending_provider_keys
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def sending_providers_project_params
    params.require(:sending_providers_project).permit(
        :sending_provider_id, :project_id, :set_default, :secret_key, :public_key,
        provider_key_values_attributes: [:id, :value, :sending_provider_key_id]
    )
  end
end
