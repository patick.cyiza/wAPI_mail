class TransactionRendererController < ActionController::Base
  def self.prepare_for_delivery params

    view = ActionView::Base.new(ActionController::Base.view_paths, {})
    class << view
      include ApplicationHelper
    end

    document = Roadie::Document.new(
          view.render(
          file: "transmit_api_mailer/prepare_for_dispatch.html.inky-haml",
          locals:{
              styling_img: params[:styling_img],
              body_to_html: params[:content_img].html_safe
          },
          layout: false
      )
    )

    document.add_css (Rails.application.assets || ::Sprockets::Railtie.build_environment(Rails.application)).find_asset("foundation_emails").to_s
    document.add_css params[:styling_img]

    document.transform.gsub(/\n/, '').gsub(/\t/, '')
  end

  def self.prepare_for_preview params
    view = ActionView::Base.new(ActionController::Base.view_paths, {})
    class << view
      include ApplicationHelper
    end

    document = Roadie::Document.new(
        view.render(
            file: "transmit_api_mailer/prepare_for_dispatch.html.inky-haml",
            locals:{
                styling_img: params[:styling_img],
                body_to_html: params[:content_img].html_safe
            },
            layout: false
        )
    )

    document.add_css (Rails.application.assets || ::Sprockets::Railtie.build_environment(Rails.application)).find_asset("foundation_emails").to_s
    document.add_css params[:styling_img]

    document.transform.gsub(/\n/, '').gsub(/\t/, '')
  end

  def self.convert_to_text html_string
    ActionController::Base.helpers.strip_tags(html_string).gsub(/ +\n/, '').gsub(/  +/, ' ')
  end
end
