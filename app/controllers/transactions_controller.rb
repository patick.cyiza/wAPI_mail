class TransactionsController < ProjectChildrenController
  before_action :set_transaction, only: [:show, :edit, :update, :destroy, :design]
  layout "projects"
  # GET /transactions
  # GET /transactions.json
  def index
    @transactions = @project.transactions.paginate(page: params[:page], per_page: 6).order(updated_at: "desc")
  end

  # GET /transactions/1
  # GET /transactions/1.json
  def show; end

  # GET /transactions/new
  def new
    @transaction = Transaction.new
    get_emails
  end

  # GET /transactions/1/edit
  def edit
    get_emails
  end

  def design; end

  # POST /transactions
  # POST /transactions.json
  def create
    @transaction = @project.transactions.build(transaction_params)

    set_code_name @transaction, transaction_params
    set_initial_mail_img @transaction

    respond_to do |format|
      if @transaction.save
        format.html { redirect_to "/projects/#{@project.id}/transactions/#{@transaction.id}/design", notice: "Transcation was successfully created." }
        format.json { render json: @transaction, status: :created }
      else
        format.html { redirect_to new_project_transaction_path, alert: @transaction.errors }
        format.json { render json: @transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /transactions/1
  # PATCH/PUT /transactions/1.json
  def update
    respond_to do |format|

      set_code_name @transaction, transaction_params

      if @transaction.update(transaction_params)
        format.html { redirect_to project_transaction_path, notice: "Transcation was successfully updated." }
        format.json { render json: @transaction, status: :ok }
      else
        format.html { redirect_to new_project_transaction_path, alert: @transaction.errors }
        format.json { render json: @transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /transactions/1
  # DELETE /transactions/1.json
  def destroy
    @transaction.destroy
    respond_to do |format|
      format.html { redirect_to project_transactions_path, notice: "Transcation was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_transaction
      @transaction = Transaction.find(params[:id])
    end

    def get_emails
      @emails = @project.sending_emails.sort_by {|mail| mail.binary_is_default?(@project.id)*-1}
      @email_arr = SendingEmail.as_array @emails
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def transaction_params
      params.require(:transaction).permit(:project_id, :sending_email_id, :template_id, :mail_body_id, :name, :content_img, :styling_img, :subject)
    end

    def set_code_name transaction, params
      if params[:name]
        transaction.code_name = Transaction.codify_string(params[:name])
      end
    end

    def set_initial_mail_img transaction
      transaction.content_img = "<!--inky html her/!-->"
      transaction.styling_img = "/* CSS code here */"
    end
end
