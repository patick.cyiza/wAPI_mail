module Api::V1::BaseApiHelper
  def generate_code_name name, id
    proccessed_name = name.gsub('.', '_').gsub('-', '_').truncate(8, omission: '')
    proccessed_id = Base64.urlsafe_encode64(id.to_s).sub!('=', '_')

    return "#{proccessed_name}_#{proccessed_id}"
  end
end
