module ApplicationHelper
  def base_url_mails
    if Rails.env.development?
      "http://0.0.0.0:3001"
    else
      "https://www.wapi_mail.com"
    end
  end
end
