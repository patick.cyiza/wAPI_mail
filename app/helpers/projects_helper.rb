module ProjectsHelper

  def project_nav_items
    [
        {label: "Project overview", path:  project_path(@project)},
        {label: "Sending email addresses", path: project_sending_emails_path(@project)},
        {label: "Transaction mails", path:  project_transactions_path(@project)},
        {label: "API security", path: "/projects/#{@project.id}/api_security"},
        {label: "Production service providers", path: project_sending_providers_projects_path(@project)}
    ]
  end

  def project_nav_link label, path, current_path
    active_result = link_to label, path, html_options = {class: "nav-link active"}

    if current_path == path
      active_result
    elsif path != project_path(@project) && current_path.include?(path)
      active_result
    else
      link_to label, path, html_options = {class: "nav-link"}
    end
  end

  def highlight_default bool
    if bool
      content_tag(:i, "check_circle", class: "material-icons", style: "color: #63b5f3")
    end
  end

  def highlight_confirmation bool
    if bool
      content_tag(:i, "check_box", class: "material-icons", style: "color: #40ba51")
    else
      content_tag(:i, "indeterminate_check_box", class: "material-icons", style: "color: #c2c2c2")
    end
  end

  def minify_content content
    content.gsub(/\n/, '').gsub(/\t/, '').gsub(/ +\n/, '').gsub(/  +/, ' ')
  end

end
