module SendingProvidersProjectsHelper

  def get_smtp_icon provider_name
    return image_tag("#{provider_name}.png", class: "rounded", style: "padding: 10px;")
  end

  def default_text is_default
    is_default ? "Current default service provider" : ""
  end

end
