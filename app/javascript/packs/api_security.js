import Vue from 'vue/dist/vue.js'
import VueResource from 'vue-resource'

import Toasted from 'vue-toasted'

//mixins
import ClipboardMixins from './mixins/clipboard'
import ToastMixins from './mixins/toast'

document.addEventListener('turbolinks:load', () => {
    const el = document.querySelector('#ApiSecuritySection');

    if (el !== null) {
        Vue.use(Toasted)
        Vue.use(VueResource)
        Vue.http.headers.common['X-CSRF-Token'] = document.querySelector('meta[name="csrf-token"]').getAttribute('content')


        const app = new Vue({
            el,
            data() {
                return {
                    project_id: JSON.parse(el.dataset.projectid),
                    dev_key: JSON.parse(el.dataset.devkey),
                    prod_key: JSON.parse(el.dataset.prodkey),
                    revealed_key: null
                }
            },

            mixins:[ ToastMixins, ClipboardMixins ],

            components: {
            },

            computed: {
                keyRing() {
                    return [this.dev_key, this.prod_key]
                },
                updatedAt() {
                    return date => new Date(date).toLocaleDateString("en-GB", {hour: '2-digit', minute:'2-digit'})
                }
            },

            methods: {
                generateKey(env) {
                    this.$http.post(
                        `/projects/${this.project_id}/widjet/v1/api_security/create_key/${env}`,
                        ""
                    ).then(resp => {
                        this.revealSecrect(resp.body,`${env} key successfully created`)
                    }).catch(error => {
                        //console.log(error.message)
                        this.closeRevealSecretModal()
                        this.toast(`Couldn't create key. Error:"${error.status} ${error.body.message}"`, "error")
                    })
                },

                changeLocks(key_id) {
                    this.revealed_key = null

                    this.$http.patch(
                        `/projects/${this.project_id}/widjet/v1/api_security/change_locks/${key_id}`,
                        ""
                    ).then(resp => {
                        this.revealSecrect(resp.body, `${resp.body.env_name} locks successfully changed`)

                    }).catch(error => {
                        //console.log(error.message)
                        this.closeRevealSecretModal()
                        this.toast(`Couldn't change locks. Error:"${error.status} ${error.body.message}"`, "error")
                    })
                },

                revealSecrect(secret_data, successMessage) {
                    try {
                        this.openRevealSecretModal()

                        if (secret_data.for_production) {
                            this.prod_key = secret_data
                        } else {
                            this.dev_key = secret_data
                        }

                        new Promise((resolve, reject) => {
                            setTimeout(() => {
                                this.revealed_key = secret_data
                                this.toast(successMessage)
                            }, 500)
                            resolve(true)
                        }).then(() => {
                            setTimeout(() => {
                                const {updated_at, env_name, client_id, secret_key} = this.revealed_key
                                const rows = [['"active_since"', '"ENV"', '"client_id"', '"secret_key"'], [`"${updated_at}"`, `"${env_name}"`, `"${client_id}"`, `"${secret_key}"`]]
                                let csvContent = "data:text/csv;charset=utf-8," + rows.map(e => e.join(",")).join("\n");
                                const encodedUri = encodeURI(csvContent)

                                let link = document.querySelector("#ddlcsv")
                                link.setAttribute("href", encodedUri)
                                link.setAttribute("download", `wapi-mail-${this.project_id}-${env_name}-${client_id}.csv`)
                                link.removeAttribute("disabled")
                            }, 1000)
                        })

                    } catch(e) {
                        this.revealed_key = null
                        setTimeout(() => {
                            this.closeRevealSecretModal()
                            this.toast(`Couldn't not reveal secret`, "error")
                        }, 500)

                    }
                },

                copyKey(attr) {
                    const value = JSON.stringify(this.revealed_key[attr]).replace(/"/g, '')
                    this.clipboardCopy(value, "#revealSecret")
                },

                openRevealSecretModal() {
                    document.querySelector('#revealSecretBtn').click()
                },

                closeRevealSecretModal() {
                    document.querySelector('#closeRevalation').click()
                },

                cleanSecret() {
                    this.revealed_key = null

                    let link = document.querySelector("#ddlcsv")
                    link.setAttribute("href", "")
                    link.setAttribute("download", "")
                    link.setAttribute("disabled", "")

                    setTimeout(() => {
                        this.closeRevealSecretModal()
                    }, 200)
                }

            },

            created() {}
        })
    }
})