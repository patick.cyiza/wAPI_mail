import Vue from 'vue/dist/vue.js'
import Cable from 'actioncable'

import Toasted from 'vue-toasted'

import MailList from './dev_mailbox/MailList'

import ToastMixins from './mixins/toast'

document.addEventListener('turbolinks:load', () => {
    const el = document.querySelector('#DevMailbox')
    const projects = JSON.parse(el.dataset.projects)

    if (el !== null && projects.length > 0) {
        Vue.use(Toasted)

        let cable = Cable.createConsumer('/cable')

        const app = new Vue({
            el,

            data() {
                return {
                    projects,
                    inbox_subs: {},
                    inboxes: [],
                    current_inbox_id: null,
                    active_mail_vector: [null, null],
                    content_iframe: null
                }
            },

            components: {
                MailList
            },

            mixins:[ ToastMixins ],

            computed: {
                currentInbox() {
                    const current = this.getInbox(this.current_inbox_id)
                    return current || {id: null, mails: []}
                },
                projectIds() {
                    return this.projects.map(project => project.id)
                }
            },

            methods:{
                getInbox(id) {
                    return this.inboxes.find(box => box.id === id)
                },
                setInbox(project) {
                    this.inboxes.push({...project, mails: []})
                },
                setMail(id, data) {
                    try {
                        const box = this.getInbox(id)

                        box.mails.push({opened: false, received_at: new Date(), data})
                        this.toast(`Mail recieved for ${box.name} successfully`)
                    } catch (e) {
                        this.toast(
                            `
                            Mail recieved but could not be handeled.
                            Error:"${error.body.message}"
                            `,
                            "error"
                        )
                        console.log(e)
                    }
                },
                subToInbox(project_id) {
                    return cable.subscriptions.create(
                        {
                            channel: "InboxNotifierChannel",
                            project_id
                        },
                        {
                            connected: () => {
                                console.log("connected to inbox-" + project_id)
                            },
                            disconnected: () => {
                                console.log("disconnected from inbox-" + project_id)
                            },
                            received: data => {
                                // console.log("recived")
                                this.setMail(project_id, data)
                            }
                        }
                    )
                },
                unsubToInbox(projet_id) {
                    this.inbox_subs["inbox_notifier" + project_id].unsubscribe()
                },
                initInboxSubs() {
                    this.projects.forEach(project => {
                        const id = project.id

                        this.setInbox(project)
                        this.inbox_subs["inbox_notifier" + id] = this.subToInbox(id)

                        const UnsubHandeler = () => {
                            this.unsubToInbox(id)
                            document.removeEventListener("turbolinks:before-render", UnsubHandeler, false)
                        }

                        document.addEventListener("turbolinks:before-render", UnsubHandeler, false)
                    })
                },
                openedMail(inbox, index) {
                    inbox.mails[index].opened = true
                },
                setActiveMail(mail_vector) {
                    const  active = this.active_mail_vector
                    const box = this.getInbox(mail_vector[0])

                    if ((active[0] !== mail_vector[0]) || (active[1] !== mail_vector[1])) {
                        this.active_mail_vector = mail_vector
                        console.log(box.mails[mail_vector[1]].data.html)
                        this.renderMail(box.mails[mail_vector[1]].data.html)
                    }

                    if (!box.mails[mail_vector[1]].opened) {
                        this.openedMail(box, mail_vector[1])
                    }
                },
                sizeIframeContent(size = null) {
                    const result = (size && `${size}px`) || "100%"
                    document.querySelector("[name=content_iframe]").style.width = result
                },
                renderMail(mail_html) {
                    document.querySelector("[name=content_iframe]").contentDocument.querySelector("html").innerHTML = mail_html
                },
                unopenedCounter(id) {
                    return this.getInbox(id).mails.filter(
                        mail => mail.opened === false
                    ).length || ""
                }
            },

            created() {
                this.initInboxSubs()
                this.current_inbox_id = this.inboxes[0].id || null
            }

        })
    }
})