import Vue from 'vue/dist/vue.js'
import VueResource from 'vue-resource'

import Toasted from 'vue-toasted'

import { codemirror } from 'vue-codemirror'
import LiveFrame from './live_preview_transaction/LiveFrame'
import LiveFrameChild from './live_preview_transaction/LiveFrameChild'
// languages
import 'codemirror/mode/xml/xml.js'
import 'codemirror/mode/css/css.js'

// require active-line.js
import'codemirror/addon/selection/active-line.js'

import ClipboardMixins from './mixins/clipboard'
import ToastMixins from "./mixins/toast";

document.addEventListener('turbolinks:load', () => {
    const el = document.querySelector('.live_design_form');

    if (el !== null) {
        Vue.use(Toasted);
        Vue.use(VueResource);
        Vue.http.headers.common['X-CSRF-Token'] = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

        const css_dependencies = el.dataset.cssdependencies;

        const app = new Vue({
            el,
            data() {
                return {
                    cc_designable: JSON.parse(el.dataset.designable),
                    designable: JSON.parse(el.dataset.designable),
                    css_dependencies: css_dependencies,
                    local_pictures: JSON.parse(el.dataset.localpictures),
                    global_pictures: JSON.parse(el.dataset.globalpictures),
                    project_id: el.dataset.projectid,
                    params_name: el.dataset.paramsname,
                    static_mode: false,
                    html_mode: true, // true reveal htlm editor, false reveal css
                    css_mode_loaded: false, // lazy load and work around a glitch
                    static_url: '',
                    image_uploading: '',
                }
            },

            mixins:[ ToastMixins, ClipboardMixins ],

            components: {
                codemirror,
                LiveFrame,
                LiveFrameChild
            },

            computed: {
                htmlOps() {
                    return {
                        tabSize: 3,
                        styleActiveLine: true,
                        lineNumbers: true,
                        autoCloseTags: true,
                        line: true,
                        connect: 'align',
                        mode: 'text/html'
                    }
                },
                cssOps() {
                    return {
                        tabSize: 2,
                        styleActiveLine: true,
                        lineNumbers: true,
                        line: true,
                        lineWrapping: true,
                        mode: 'text/css',
                        theme: 'base16-light'
                    }
                },
                updatedAt() {
                    var date = new Date(this.designable.updated_at);
                    return date.toLocaleDateString("en-GB", {hour: '2-digit', minute:'2-digit'})
                },
                inkedHtml() {
                    var Inky = require('inky/lib/inky.js');
                    var inky = new Inky();

                    return inky.releaseTheKraken(this.parseAssetInterpolation(this.designable.content_img))
                },
                cssAssetParser() {
                    return this.parseAssetInterpolation(this.designable.styling_img)
                },
                designableType() {
                    return `${this.params_name}s`
                },
                allPicturesArray() {
                    return this.local_pictures.concat(this.global_pictures)
                },
                assetInterpolation() {
                    return codename => `@!${codename}!`
                }
            },

            methods: {
                toggleEditor(mode) {
                    if(mode !== this.html_mode) this.html_mode = !this.html_mode;
                    if(this.css_mode_loaded !== true) this.css_mode_loaded = true
                },
                pictureUpload(file, endpoint, collection) {
                    const fileData = new FormData();
                    fileData.append("sendable_asset[picture_attributes][illustration]", file);
                    fileData.append("sendable_asset[code_name]", file.name);
                    this.$http.post(endpoint,
                        fileData,
                        { headers: { 'Content-Type': 'multipart/form-data' } }
                    ).then(resp => {
                        collection.push(resp.body);
                        this.toast("picture successfully added");
                        this.image_uploading = "";
                    }).catch(error => {
                        console.log("error", error);
                        this.toast(`Couldn't add picture. Error:"${error.status} ${error.body.message}"`, "error")
                        this.image_uploading = "";
                    })
                },
                localPictureUpload(event) {
                    const file = event.target.files[0];
                    const endpoint = `/projects/${this.project_id}/widjet/v1/designer/${this.designableType}/${this.designable.id}/upload_picture_asset`;
                    this.image_uploading = "local";
                    if(!!file) {
                        this.pictureUpload(file, endpoint, this.local_pictures)
                    }
                },
                globalPictureUpload(event) {
                    const file = event.target.files[0];
                    const endpoint = `/projects/${this.project_id}/widjet/v1/designer/upload_picture_asset`;
                    this.image_uploading = "global";
                    if(!!file) {
                        this.pictureUpload(file, endpoint, this.global_pictures)
                    }
                },
                confirmDeletePic(asset_id, asset_scope) {
                    this.$toasted.show("Are you sure you want to delete this picture?", {
                        theme: "bubble",
                        position: "bottom-center",
                        duration : 10000,
                        action : [
                            {
                                text : 'No',
                                onClick: (e, toastObject) => {
                                    toastObject.goAway(0)
                                }
                            },
                            {
                                text: 'yes',
                                onClick: (e, toastObject) => {
                                    toastObject.goAway(0);
                                    this.deletePicture(asset_id, asset_scope)
                                }
                            }
                        ]
                    })
                },

                deletePicture(asset_id, asset_scope) {
                    this.$http.delete(
                        `/projects/${this.project_id}/widjet/v1/designer/delete_asset/${asset_id}`
                    ).then(resp => {
                        this[`${asset_scope}_pictures`] = this[`${asset_scope}_pictures`].filter(pic => pic.id !== asset_id);
                        event.target.value = "";
                        this.toast("Picture successfully deleted")
                    }).catch(error => {
                        console.log("error", error);
                        this.toast(`Couldn't delete picture. Error:"${error.status} ${error.body.message}"`, "error")
                    })
                },
                copyAssetInterpolation(codename) {
                    const value = this.assetInterpolation(codename);
                    this.clipboardCopy(value)
                },
                parseAssetInterpolation(codeImg) {
                    const rawVarsArr = codeImg.match(/@!+\w+=*!/gi); // get substrings where a asset interpolation is used
                    const cleanArr = [...new Set(rawVarsArr)];
                    let parsedCode = codeImg;

                    for (let interpoled of cleanArr ) {
                        let picture = this.allPicturesArray.find(pic => pic.code_name === interpoled.replace(/[@! ]/gi, ""));
                        if (!!picture) {
                            parsedCode = parsedCode.replace(new RegExp(interpoled, 'gi'), picture.img)
                        }
                    }
                    return parsedCode
                },
                seeStaticPreview() {
                    const from  = el.dataset.from;

                    this.$http.post(
                        `/projects/${this.project_id}/widjet/v1/designer/preview`,
                        `{"previewer": {"from": "${from}", "subject": "${this.designable.subject}", "content_img": ${JSON.stringify(this.inkedHtml)}, "styling_img": ${JSON.stringify(this.cssAssetParser)}}}`
                    ).then(response => {
                        this.static_mode = true;
                        this.static_url = response.body.result_path
                    }).catch(error => {
                        this.toast(`Couldn't load stastic preview. Error:"${error.status} ${error.statusText}"`, "error")
                    })
                },
                saveEdits() {
                    this.$http.patch(
                        `/projects/${this.project_id}/widjet/v1/designer/${this.designableType}/${this.designable.id}`,
                        `{"designable": ${JSON.stringify(this.designable)}}`
                    ).then(response => {
                        this.designable.updated_at = response.data.updated_at;
                        this.cc_designable.content_img = response.data.content_img;
                        this.cc_designable.styling_img = response.data.styling_img;
                        this.cc_designable.subject = response.data.subject;
                        this.toast(`${this.params_name} successfully saved`)
                    }, error => {
                        this.toast(`Couldn't save ${this.params_name}. Error:"${error.status} ${error.body.message}"`, "error")
                    })
                }
            },

            created() {

            }
        })

    }

});