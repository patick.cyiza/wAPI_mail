export default {
    methods: {
        clipboardCopy(value, foreground_selector = "body") {
            try {
                const foreground_node = document.querySelector(foreground_selector)

                const clip = document.createElement('textarea')
                clip.setAttribute('readonly', '')
                clip.style.position = 'absolute'
                clip.style.left = '-9999px'
                foreground_node.appendChild(clip)
                clip.value = value
                clip.select()

                document.execCommand('copy')
                foreground_node.removeChild(clip)

                this.toast("Copied succesfully.", "info", "bottom-left")
            } catch(err) {
                this.toast(`Copy failed reason: ${err.message}`, "info", "bottom-left")
            }
        }
    }
}