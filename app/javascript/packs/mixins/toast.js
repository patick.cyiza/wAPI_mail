export default {
    methods: {
        toast(msg, type = "success", position = "top-left"){
            this.$toasted.show(msg,
                {
                    theme: "bubble",
                    type: type,
                    position: position,
                    fitToScreen: true,
                    duration : 5000
                }
            )
        }
    }
}