class BroadcastToDevInboxJob < ActiveJob::Base
  queue_as :default

  def perform(project_id, transaction_id, mail_content, correspondents)
    ActionCable.server.broadcast("inbox-#{project_id}",
        transaction_id: transaction_id,
        from: correspondents[:from],
        to: correspondents[:to],
        subject: mail_content[:subject],
        html: TransactionRendererController.prepare_for_delivery(mail_content)
    )
  end
end
