class Dispatch::MailgunJob < DispatchJob
  require 'net/http'
  require 'uri'


  def perform(credentials, mail_content, correspondents)
    begin
      uri = URI.parse("https://api.eu.mailgun.net/v3/#{credentials['sending_domain']}/messages")
      html_string = TransactionRendererController.prepare_for_delivery(mail_content)

      body = {
          from: correspondents[:from],
          to: correspondents[:to],
          subject: mail_content[:subject],
          text: TransactionRendererController.convert_to_text(html_string),
          html: html_string
      }

      http = Net::HTTP.new(uri.host, uri.port)
      request = Net::HTTP::Post.new(uri.request_uri)
      http.use_ssl = true
      request.basic_auth("api", credentials['api_key'])
      request.set_form_data(body)

      response = http.request(request)

      case response.code
        when 200
          p response.as_json
        when 400..499
          p "Error #{response.message}"
        when 500..599
          p "Error #{response.message}: try again later?"
        else
          p response.message
      end

      GC.start
    rescue => e
      raise e
    end
  end
end
