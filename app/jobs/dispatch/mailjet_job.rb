class Dispatch::MailjetJob < DispatchJob
  require 'net/http'
  require 'uri'

  def perform(credentials, mail_content, correspondents)
    begin
      uri = URI.parse("https://api.mailjet.com/v3.1/send")
      header = {"Content-Type": "application/json"}
      html_string = TransactionRendererController.prepare_for_delivery(mail_content)

      body = {"Messages" => [
          {
             'From'=> {
                 'Email'=> correspondents[:from],
                 'Name'=> ''
             },
             'To'=> [{'Email'=> correspondents[:to], 'Name'=> ''}],
             'Subject'=> mail_content[:subject],
             'TextPart'=> TransactionRendererController.convert_to_text(html_string),
             'HTMLPart'=> html_string
          }
        ]
      }

      http = Net::HTTP.new(uri.host, uri.port)
      request = Net::HTTP::Post.new(uri.request_uri, header)
      http.use_ssl = true
      request.basic_auth(credentials['api_key'], credentials['secret_key'])
      request.body = body.to_json

      response = http.request(request)

      p response.as_json

      GC.start
    rescue => e
      raise e
    end

  end

end
