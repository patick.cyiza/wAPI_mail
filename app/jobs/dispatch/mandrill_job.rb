class Dispatch::MandrillJob < DispatchJob
  require 'mandrill'

  def perform(credentials, mail_content, correspondents)
    begin
      mandrill = Mandrill::API.new credentials['api_key']

      message = {
           "to"=>
           [{
              "name"=>"",
              "type"=>"to",
              "email"=> correspondents[:to]
            }],
           "headers"=>{"Reply-To"=> correspondents[:from]},
           "merge_language"=>"mailchimp",
           "from_name"=>"",
           "subject"=> mail_content[:subject],
           "html"=> TransactionRendererController.prepare_for_delivery(mail_content),
           "merge"=>true,
           "from_email"=>correspondents[:from]
      }

      response = mandrill.messages.send message, false, "", ""
      p response.as_json

      GC.start
    rescue Mandrill::Error => e
      raise e
    end
  end
end
