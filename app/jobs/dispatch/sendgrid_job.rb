class Dispatch::SendgridJob < DispatchJob
  require 'sendgrid-ruby'
  include SendGrid

  def perform(credentials, mail_content, correspondents)
    begin

      from = SendGrid::Email.new(email: correspondents[:from])
      to = SendGrid::Email.new(email: correspondents[:to])
      subject = mail_content[:subject]
      content = SendGrid::Content.new(type: 'text/html', value: TransactionRendererController.prepare_for_delivery(mail_content))
      mail = SendGrid::Mail.new(from, subject, to, content)

      sg = SendGrid::API.new(api_key: credentials['api_key'])
      response = sg.client.mail._('send').post(request_body: mail.to_json)

      case response.status_code
      when 200..299
        p response.as_json
      when 400..499
        p "Error #{response}"
      when 500..599
        p "Error #{response}: try again later?"
      else
        p response.as_json
      end

      GC.start
    rescue => e
      raise e
    end
  end
end
