class Dispatch::SprakpostJob < DispatchJob
  require 'net/http'
  require 'uri'

  def perform(credentials, mail_content, correspondents)
    begin
      uri = URI.parse("https://api.sparkpost.com/api/v1/transmissions")
      header = {"Authorization": credentials['api_key'], "Content-Type": "application/json"}
      html_string = TransactionRendererController.prepare_for_delivery(mail_content)

      body = {
        'content': {
            'from': correspondents[:from],
            'subject': mail_content[:subject],
            'text': TransactionRendererController.convert_to_text(html_string),
            'html': html_string
        },
        'recipients': [{'address': correspondents[:to]}],
      }

      http = Net::HTTP.new(uri.host, uri.port)
      request = Net::HTTP::Post.new(uri.request_uri, header)
      http.use_ssl = true
      request.body = body.to_json

      response = http.request(request)

      p response.as_json

      GC.start
    rescue => e
      raise e
    end

  end

end
