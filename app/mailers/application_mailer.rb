class ApplicationMailer < ActionMailer::Base
  default from: Rails.application.secrets.default_sender
  layout 'internal_transaction_layout'
  helper EmailHelper
end
