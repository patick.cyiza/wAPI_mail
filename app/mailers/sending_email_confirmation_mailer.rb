class SendingEmailConfirmationMailer < ApplicationMailer
  helper :application
  default template_path: 'sending_emails/mailer' # to make sure that your mailer uses the devise views

  def send_confirmation (project, sending_email, token)
    @project = project
    @sending_mail = sending_email
    @token = token

    mail(
        to: @sending_mail.email,
        subject: "Confirm ownership of email #{@sending_mail.email}"
    )
  end

end
