class TransmitApiMailer < ActionMailer::Base

  helper :application # gives access to all helpers defined within `application_helper`.

  def prepare_for_preview mail_content, correspondents
    @body_to_html = mail_content[:content_img].gsub(/>+[\W\s\\n]+</, "><").html_safe
    @styling_img = mail_content[:styling_img]

    mail(
        from: correspondents[:from],
        to: correspondents[:to],
        subject: mail_content[:subject]
    )

  end
end
