class Defaulty < ApplicationRecord
  belongs_to :project
  belongs_to :defaultable, polymorphic: true
  validates :project, uniqueness: { scope: :defaultable_type }

  def self.get_defaulty(project_id, defaulty_type)
    self.where(defaultable_type: defaulty_type).find_by(project_id: project_id)
  end

end
