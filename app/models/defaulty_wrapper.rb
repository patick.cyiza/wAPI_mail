module DefaultyWrapper

  def self.included(base)
    base.has_many :defaulties, as: :defaultable, dependent: :destroy
    attr_accessor :set_default
  end

  def binary_is_default? project_id
    result = 0
    if Project.find(project_id).has_as_default? self
      result = 1
    end
    return result
  end

  def update_or_create_default(defaultable = self)
    project = defaultable.project
    defaulty = project.get_defaulty(defaultable.class.name)
    if defaulty
      if defaulty.defaultable != defaultable
        defaulty.update(defaultable_id: defaultable.id)
      end
    else
      Defaulty.create(project: project, defaultable: defaultable)
    end
  end

end