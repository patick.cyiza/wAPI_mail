module DesignableWrapper

  def self.included(base)
    base.has_many :sendable_assets, as: :attachable, dependent: :destroy
    base.has_many :designables_variables, as: :editable, dependent: :destroy
    base.accepts_nested_attributes_for :designables_variables, reject_if: :all_blank, allow_destroy: true
  end

  def get_params_name
    self.class.name.downcase.singularize
  end

  def self.get_designable table_name, id
    table_name.classify.constantize.find(id)
  end

  def get_pictures
    self.sendable_assets.of_pictures
  end


end
