class DesignablesVariable < ApplicationRecord
  belongs_to :editable, polymorphic: true
end
