class MailBody < ApplicationRecord
  belongs_to :user
  has_many :transactions

  include DesignableWrapper
end
