class Picture < ApplicationRecord
  belongs_to :illustrable, polymorphic: true, optional: true

  #paperclip config
  has_attached_file :illustration, styles: { cover: "700x700>",medium: "300x300>", thumb: "100x100>", tiny: "50x50>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :illustration, content_type: /\Aimage\/.*\z/
end
