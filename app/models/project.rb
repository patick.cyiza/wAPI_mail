class Project < ApplicationRecord
  belongs_to :user
  has_many :transactions, dependent: :destroy
  has_many :templates, dependent: :destroy
  has_many :sending_providers_projects, dependent: :destroy
  has_many :sending_emails, dependent: :destroy
  has_many :defaulties, dependent: :destroy
  has_many :project_api_keys, dependent: :destroy
  has_one :picture, as: :illustrable, dependent: :destroy
  has_many :sendable_assets, as: :attachable, dependent: :destroy

  validates :name, uniqueness: { scope: :user_id }

  # nested attributes
  accepts_nested_attributes_for :picture, reject_if: :all_blank, allow_destroy: true

  def logo scale = :thumb
    if self.picture
      self.picture.illustration scale
    else
      "default_logo.jpg"
    end
  end

  def get_defaulty defaulty_type
    self.defaulties.get_defaulty(self.id, defaulty_type)
  end

  def get_default_of defaulty_type
    defaultables = Object.const_get(defaulty_type).where(project_id: self.id)
    defaulty = self.get_defaulty defaulty_type
    if  defaulty
      defaultables.find(defaulty.defaultable_id)
    else
      defaultables.first
    end
  end

  def has_as_default? defaultable
    result = false
    if self.get_default_of(defaultable.class.name) == defaultable
      result = true
    end
    return result
  end

  def get_dev_key
    key = self.project_api_keys.find_by(for_production: false)

    key || ProjectApiKey.new(for_production: false)
  end

  def get_prod_key
    key = self.project_api_keys.find_by(for_production: true)

    key || ProjectApiKey.new(for_production: true)
  end

  def generate_send_api_dev_key
    current_key = self.get_dev_key
    if self.get_dev_key.id == nil
      ProjectApiKey.generate_keys self, false
    else
      current_key.change_locks
    end
  end

  def generate_send_api_prod_key
    current_key = self.get_prod_key
    if current_key.id == nil
      ProjectApiKey.generate_keys self, true
    else
      current_key.change_locks
    end
  end

  def change_locks_send_api_dev
    self.get_dev_key.change_locks
  end

  def change_locks_send_api_prod
    self.get_prod_key.change_locks
  end

  def unused_smtp_providers
    results = []
    providers = self.sending_providers_projects.map{|a| SendingProvider.find(a.sending_provider_id)}
    SendingProvider.all.each do |sp|
      if providers.exclude? sp
        results << sp
      end
    end
    return results
  end

  def provider_credentials
    self.get_default_of("SendingProvidersProject").get_credentials
  end

end
