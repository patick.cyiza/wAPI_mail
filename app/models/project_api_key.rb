class ProjectApiKey < ApplicationRecord
  belongs_to :project
  validates :for_production, uniqueness: { scope: :project_id}, allow_nil: false
  validates :client_id, uniqueness: true, presence: true
  validates :public_key, uniqueness: true, presence: true

  def public_RSA
    OpenSSL::PKey::RSA.new(self.public_key)
  end


  def self.generate_keys project_, forproduction_
    keys = JwtService.request_RSA_certs
    key_instance = ProjectApiKey.new(
      project: project_,
      public_key: keys[:RSA][:public_key],
      client_id: ProjectApiKey.make_client_id,
      for_production: forproduction_
    )

    if key_instance.save!
      key_instance.reveal_secret_hash keys[:RSA][:private_key]
    else
      false
    end
  end

  def change_locks
    keys = JwtService.request_RSA_certs

    if self.update(public_key: keys[:RSA][:public_key], client_id: ProjectApiKey.make_client_id)
      reveal_secret_hash keys[:RSA][:private_key]
    else
      false
    end
  end

  def env_name
    self.for_production ? "PRODUCTION" : "DEVELOPMENT"
  end

  def computed_json
    self.to_json(methods: :env_name)
  end

  def self.make_client_id
    result = ""
    1.times do
      proposal = SecureRandom.urlsafe_base64(5)
      if ProjectApiKey.find_by(client_id: proposal) == nil
        result = proposal
      else
        redo
      end
    end
    return result
  end

  def reveal_secret_hash secret_key
    secret_hash = self.attributes
    secret_hash[:secret_key] = secret_key
    secret_hash[:env_name] = self.env_name
    return secret_hash
  end

end
