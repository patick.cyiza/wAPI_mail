class ProviderKeyValue < ApplicationRecord
  belongs_to :sending_providers_project, optional: true
  belongs_to :sending_provider_key

  attr_encrypted :value, key: Base64.decode64(Rails.application.secrets.service_provider_secret)

  validates :sending_providers_project, uniqueness: { scope: :sending_provider_key }
end
