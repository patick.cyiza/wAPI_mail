class PublicTemplate < ApplicationRecord
  belongs_to :user

  include DesignableWrapper
end
