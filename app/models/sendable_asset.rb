class SendableAsset < ApplicationRecord
  belongs_to :attachable, polymorphic: true
  has_one :picture, as: :illustrable, dependent: :destroy

  validates :code_name, uniqueness: { scope: :attachable, message: "name or look alike already taken" }

  # nested attributes
  accepts_nested_attributes_for :picture, reject_if: :all_blank, allow_destroy: true

  def self.of_pictures
    self.where(asset_type: "Picture")
  end

  def self.to_widjet_json
    result =  []
    self.of_pictures.map do |pic|
      result << {id: pic.id, code_name: pic.code_name, thumb: pic.get_image(:tiny), img: pic.get_image}
    end
    return result.to_json
  end

  def get_image scale = nil
    if self.picture
      picture.illustration scale
    else
      nil
    end
  end

  def get_image_url scale = nil
    if Rails.env.development?
      'http://localhost:3001' + get_image(scale)
    else
      get_image(scale)
    end
  end

  def predicted_id
    self.class.maximum(:id).to_i+1
  end

end
