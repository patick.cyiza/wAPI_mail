class SendingEmail < ApplicationRecord
  belongs_to :project
  has_many :transactions
  validates_uniqueness_of :email
  validates_format_of :email, with: Devise::email_regexp

  include DefaultyWrapper

  def create_confirmation_token
    payload = {email: self.email}
    JwtService.encode_confirmation_token(payload)
  end

  def self.confirm_by_token(token)
    payload = JwtService.decode_confirmation_token(token)
    if payload
      sending_email = SendingEmail.find_by(email: payload["email"])
      if sending_email && Time.now < Time.at(payload["exp"])
        sending_email.update_column('confirmed_at', Time.now)
        return sending_email
      end
    end
  end

  def is_confirmed?
    result = false
    if self.confirmed_at && self.updated_at < self.confirmed_at
      result = true
    end
    return result
  end

  def self.as_array (selection)
    arr = []
    selection.map{|mail| arr << [mail.email, mail.id] }
    return arr
  end

end
