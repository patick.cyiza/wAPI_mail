class SendingProvider < ApplicationRecord
  has_many :sending_providers_projects, dependent: :destroy
  has_many :sending_provider_keys, dependent: :destroy

  def self.enum_build_states
    return [ :in_development, :beta, :supported ]
  end

  def keys
    self.sending_provider_keys
  end

end
