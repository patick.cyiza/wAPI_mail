class SendingProviderKey < ApplicationRecord
  belongs_to :sending_provider
  has_many :provider_key_values, dependent: :destroy

  validates :sending_provider, uniqueness: { scope: :key_name }
end
