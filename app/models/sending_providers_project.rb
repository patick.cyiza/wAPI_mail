class SendingProvidersProject < ApplicationRecord
  belongs_to :sending_provider
  belongs_to :project
  has_many :provider_key_values, dependent: :destroy

  validates :project, uniqueness: { scope: :sending_provider }
  include DefaultyWrapper

  accepts_nested_attributes_for :provider_key_values, reject_if: :all_blank, allow_destroy: false

  def keys
    sending_provider.keys
  end

  def new_values_for_keys
    self.keys.map do |key|
      ProviderKeyValue.find_by(sending_providers_project: self, sending_provider_key: key) || ProviderKeyValue.new(sending_providers_project: self, sending_provider_key: key)
    end
  end

  def get_credentials
    creds = {"provider" => self.sending_provider.name}
    self.keys.each do |k|
      creds[k.key_name] = ProviderKeyValue.find_by(sending_provider_key: k, sending_providers_project: self).value
    end
    return creds
  end

end
