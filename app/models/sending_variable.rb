class SendingVariable < ApplicationRecord
  belongs_to :user
  has_many :designables_variables, dependent: :destroy
end
