class Template < ApplicationRecord
  belongs_to :project
  has_many :transcations

  include DefaultyWrapper
  include DesignableWrapper
end
