class Transaction < ApplicationRecord
  belongs_to :project
  belongs_to :sending_email
  belongs_to :template, optional: true
  belongs_to :mail_body, optional: true

  validates :name, format: {
      with: /^[a-zA-Z0-9 ]*$/,
      multiline: true,
      message: "no special characters allowed, only letters and numbers"
  }
  validates :code_name, uniqueness: { scope: :project_id, message: "name or look alike already taken" }

  include DesignableWrapper

  def self.codify_string str
    str.underscore.gsub(" ", "_" ).gsub("-", "_" )
  end

  def codify_name
    Transaction.codify_string self.name
  end

  def content_img_minified
    self.content_img.gsub(/>+[\W\s\\n]+</, '><').gsub(/  +/, ' ')
  end

  def styling_img_minified
    self.styling_img.gsub(/\\n/, '').gsub(/\\t/, '')
  end

  def parse_transaction params
    result_hash = {
      subject: self.subject,
      # minify the content, make it less heavy in trasmission
      content_img: self.content_img_minified,
      styling_img: self.styling_img_minified
    }

    # parsing assets in the transaction content and styling image
    asset_parsed = asset_parser(result_hash)
    result_hash[:styling_img] = asset_parsed[:styling_img]
    result_hash[:content_img] = asset_parsed[:content_img]

    # parsing iterators in transaction content image
    result_hash[:content_img] = self.iterator_parser(result_hash[:content_img], params)

    # parsing parsing primitives variables in transaction content and subject
    variable_parsed = variable_parser(result_hash, params)
    result_hash[:subject] = variable_parsed[:subject]
    result_hash[:content_img] = variable_parsed[:content_img]

    return result_hash
  end

  def preview_html_string
    if self.content_img && self.content_img != "<!--inky html here></!-->"
      result_hash = {
          subject: self.subject,
          # minify the content, make it less heavy in trasmission
          content_img: self.content_img_minified,
          styling_img: self.styling_img_minified
      }

      # parsing assets in the transaction content and styling image
      asset_parsed = asset_parser(result_hash)

      TransactionRendererController.prepare_for_preview(asset_parsed)
    else
      ""
    end
  end

  def mail_from
    self.sending_email.email
  end

  def self.clean_interpolations raw_images, finder_regex, cleaner_regex
    interpolations = []

    raw_images.each{ |img| interpolations.concat(img.scan(finder_regex)) }

    return interpolations.uniq.map{|i| i.gsub(cleaner_regex, '')}
  end

  def designable_props
    return {subject: self.subject, content_img: self.content_img, styling_img: self.styling_img}
  end

  def get_vars_keys
    # /%{+\w+}/ is the regex of the chosen string interpolation synthax e.g. %{variable}%
    # this methods extracts all those variable name and put in a array
    # it also takes care of having just one occurence of each viarable
    # eventhough it might be declared multiple times in self.content_img
    Transaction.clean_interpolations [self.subject, self.content_img], /%{+\w+}/, /[%{} ]/
  end

  def variable_parser hash_to_parse, params
    resulted_hash = {subject: hash_to_parse[:subject], content_img: hash_to_parse[:content_img]}

    self.get_vars_keys.each do |var|
      resulted_hash[:subject] = resulted_hash[:subject].gsub("%{#{var}}", params[:"#{var}"])
      resulted_hash[:content_img] = resulted_hash[:content_img].gsub("%{#{var}}", params[:"#{var}"])
    end

    return resulted_hash
  end

  def get_assets_interpolations
    # this is for assets interpolation a liltle bit the same story as above
    Transaction.clean_interpolations [self.styling_img, self.content_img], /@!+\w+=*!/,/[@! ]/
  end

  def asset_parser hash_to_parse
    resulted_hash = {styling_img: hash_to_parse[:styling_img], content_img: hash_to_parse[:content_img]}

    available_assets = self.sendable_assets + self.project.sendable_assets

    self.get_assets_interpolations.each do |i|

      if asset = available_assets.find{|a| a.code_name == i }
        resulted_hash[:styling_img] = resulted_hash[:styling_img].gsub("@!#{i}!", asset.get_image_url)
        resulted_hash[:content_img] = resulted_hash[:content_img].gsub("@!#{i}!", asset.get_image_url)
      end
    end

    return resulted_hash
  end

  def get_iterators_interpolations
    # same logic again to spot intended iterators.
    # clean_interpolation class method ends up being pretty useful
    Transaction.clean_interpolations [self.content_img_minified], /<\s*iter%+\s+\w+/, "<iter% "
  end

  def iterator_parser content_img, params
    result_html = content_img

    self.get_iterators_interpolations.each do |iterator|

      content_img.scan(/<iter%[^>]+\b#{iterator}+>([^$]+?)<\/iter%>/).each do |raw_content|
        # for some reason the parenthesis in the ([^$]+?) of the regex above
        # makes the scan method do nested arrays for each the elements found
        # with the founded element as the only item in the nested array
        processed_content = ""
        iterator_attrs = Transaction.clean_interpolations raw_content, /%{i\.+\w+}/, /i+\.|[%{} ]/

        params[:"#{iterator}"].each do |item|
          # raw_content[0] cause nested array, remember.... see comments above
          item_content = raw_content[0]

          iterator_attrs.each do |attr|
            item_content = item_content.gsub("%{i.#{attr}}", item[:"#{attr}"])
          end
          processed_content.concat(item_content)
        end

        result_html = result_html.sub(raw_content[0], processed_content)

      end
    end

    return result_html.gsub(/<\s*iter%+\s+\w+>|<\/iter%>/, "")
  end

end
