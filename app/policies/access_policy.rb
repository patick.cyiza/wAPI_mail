class AccessPolicy
  include AccessGranted::Policy

  def configure

    role :member, proc { |user| user.confirmed? } do
       can :crud, Project do |project, user|
         user.is_project_owner?(project)
       end
    end

  end
end
