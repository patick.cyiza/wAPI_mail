module JwtService

  extend self
  # zip preview data
  def params_zipper(params)
    return JWT.encode params, Rails.application.secrets.jwt_secret
  end

  def params_unzipper(params)
    return JWT.decode(params, Rails.application.secrets.jwt_secret).first.deep_symbolize_keys!
  end

  # jwt for to confirm sending_email
  def encode_confirmation_token(payload, expiration = nil)
    expiration ||= Rails.application.secrets.jwt_expiration_hours

    payload = payload.dup
    payload['exp'] = expiration.to_i.hours.from_now.to_i

    JWT.encode payload, Rails.application.secrets.jwt_secret
  end

  def decode_confirmation_token token
    begin
      decoded_token = JWT.decode token, Rails.application.secrets.jwt_secret

      return decoded_token.first
    rescue
      nil
    end
  end

  def request_RSA_certs
    rsa_private = OpenSSL::PKey::RSA.generate 2048
    rsa_public = rsa_private.public_key

    return {"RSA": {private_key: rsa_private.to_s, public_key: rsa_public.to_s}}
  end

  def send_api_encode_token id, transaction_data, secret
    secret_RSA = OpenSSL::PKey::RSA.new(secret)
    payload = { client_id: id, exp: (Time.now + 1.minutes).to_i, transaction: transaction_data }

    return JWT.encode payload, secret_RSA, 'RS256'
  end

  def get_authorized_payload token
    # the params for the mail are in the JWT so this data keeps its integrity
    begin
      id = JWT.decode(token, nil, false).first['client_id']
      key = ProjectApiKey.find_by(client_id: id)
      signed_token = JWT.decode(token, key.public_RSA, true, { algorithm: 'RS256' })
      if signed_token.first['exp'] < (Time.now + 1.minutes).to_i
        return {key: key, transaction_params: signed_token.first['transaction'].deep_symbolize_keys!}
      else
        false
      end
    rescue JWT::ExpiredSignature
      nil
    end
  end

end