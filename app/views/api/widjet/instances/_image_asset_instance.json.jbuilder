json.id asset.id
json.code_name asset.code_name
json.thumb asset.get_image(:thumb)
json.img asset.get_image