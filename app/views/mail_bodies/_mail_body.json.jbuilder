json.extract! mail_body, :id, :user_id, :name, :content_img, :styling_img, :created_at, :updated_at
json.url mail_body_url(mail_body, format: :json)
