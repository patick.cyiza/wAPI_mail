json.extract! public_template, :id, :content_img, :styling_img, :created_at, :updated_at
json.url public_template_url(public_template, format: :json)
