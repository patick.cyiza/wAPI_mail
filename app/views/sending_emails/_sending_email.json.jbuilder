json.extract! sending_email, :id, :project_id, :email, :is_default, :created_at, :updated_at
json.url sending_email_url(sending_email, format: :json)
