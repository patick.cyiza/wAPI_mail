json.extract! sending_providers_project, :id, :sending_provider_id, :project_id, :is_default, :secret_key, :public_key, :created_at, :updated_at
json.url sending_providers_project_url(sending_providers_project, format: :json)
