json.extract! template, :id, :project_id, :name, :content_img, :styling_img, :is_default, :created_at, :updated_at
json.url template_url(template, format: :json)
