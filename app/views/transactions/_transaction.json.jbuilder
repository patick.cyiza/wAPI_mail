json.extract! transaction, :id, :project_id, :sending_email_id, :template_id, :mail_body_id, :name, :content_img, :styling_img, :created_at, :updated_at
json.url transaction_url(transaction, format: :json)
