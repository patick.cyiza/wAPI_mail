Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  resources :mail_bodies
  resources :public_templates

  resources :projects do
    resources :transactions
    resources :templates
    resources :sending_emails
    resources :sending_providers_projects
  end

  root to: 'application#home'
  devise_for :users, :controllers => {:registrations => "registrations"}
  resources :users

  # pages
  get 'home', to: 'application#home'
  get '/dev_mailbox', to: 'dev_mailbox#index'
  get '/projects/:project_id/confirm_sending_emails', to: 'sending_emails#confirm_email'
  get '/projects/:project_id/sending_emails/:id/resend_confirmation', to: 'sending_emails#resend_confirmation'
  get '/projects/:project_id/transactions/:id/design', to: 'transactions#design'
  get '/projects/:id/api_security', to: 'projects#api_security'
  get '/projects/:project_id/create_from_provider/:sending_provider_id', to: 'sending_providers_projects#create_from_provider'

  # vue widget Api
  scope '/projects/:project_id/widjet/v1' do
    # designer endpoint
    scope '/designer' do
      post '/preview', to: 'api/widjet/designer#preview'
      post '/:designable_type/:id/upload_picture_asset', to: 'api/widjet/designer#create_local_picture_asset'
      post '/upload_picture_asset', to: 'api/widjet/designer#create_global_picture_asset'
      patch '/:designable_type/:id', to: 'api/widjet/designer#update_designable'
      delete '/delete_asset/:sendable_asset_id', to: 'api/widjet/designer#destroy_asset'
    end

    # Api security endpoint
    scope '/api_security' do
      post '/create_key/:env', to: 'api/widjet/api_security#generate_key'
      patch '/change_locks/:id', to: 'api/widjet/api_security#change_locks'
    end
  end

  # client Api endpoints
  scope '/v1', defaults: {format: :json} do
    post '/transmit/:code_name', to: 'api/v1/transmit_api#transmit_transaction'
  end


end

