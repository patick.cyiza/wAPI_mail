const { environment } = require('@rails/webpacker')
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const vue = require('./loaders/vue')
const sass = require('./loaders/sass')

environment.loaders.append('vue', vue)
environment.loaders.append('sass', sass)
environment.plugins.append('VueLoaderPlugin', new VueLoaderPlugin())
module.exports = environment
