class CreateSendingServices < ActiveRecord::Migration[5.1]
  def change
    create_table :sending_services do |t|
      t.string :name
      t.string :sending_controller_url
      t.string :api_access_docs_url

      t.timestamps
    end
  end
end
