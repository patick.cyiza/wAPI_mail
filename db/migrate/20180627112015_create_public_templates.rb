class CreatePublicTemplates < ActiveRecord::Migration[5.1]
  def change
    create_table :public_templates do |t|
      t.text :content_img
      t.text :styling_img

      t.timestamps
    end
  end
end
