class CreateSendingServicesProjects < ActiveRecord::Migration[5.1]
  def change
    create_table :sending_services_projects do |t|
      t.references :sending_service, foreign_key: true
      t.references :project, foreign_key: true
      t.boolean :is_default
      t.string :secret_key
      t.string :public_key

      t.timestamps
    end
  end
end
