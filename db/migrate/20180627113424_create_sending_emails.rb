class CreateSendingEmails < ActiveRecord::Migration[5.1]
  def change
    create_table :sending_emails do |t|
      t.references :project, foreign_key: true
      t.string :email
      t.boolean :is_default

      t.timestamps
    end
  end
end
