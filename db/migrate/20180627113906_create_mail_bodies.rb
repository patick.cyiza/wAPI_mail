class CreateMailBodies < ActiveRecord::Migration[5.1]
  def change
    create_table :mail_bodies do |t|
      t.references :user, foreign_key: true
      t.string :name
      t.text :content_img
      t.text :styling_img

      t.timestamps
    end
  end
end
