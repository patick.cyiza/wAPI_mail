class CreateTemplates < ActiveRecord::Migration[5.1]
  def change
    create_table :templates do |t|
      t.references :project, foreign_key: true
      t.string :name
      t.text :content_img
      t.text :styling_img
      t.boolean :is_default

      t.timestamps
    end
  end
end
