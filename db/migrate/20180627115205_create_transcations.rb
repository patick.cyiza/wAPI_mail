class CreateTranscations < ActiveRecord::Migration[5.1]
  def change
    create_table :transcations do |t|
      t.references :project, foreign_key: true
      t.references :sending_email, foreign_key: true
      t.references :template, foreign_key: true
      t.references :mail_body, foreign_key: true
      t.string :name
      t.text :content_img
      t.text :styling_img

      t.timestamps
    end
  end
end
