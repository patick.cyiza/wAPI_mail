class CreateEditablesVariables < ActiveRecord::Migration[5.1]
  def change
    create_table :editables_variables do |t|
      t.references :sending_variable, foreign_key: true
      t.references :editable, polymorphic: true

      t.timestamps
    end
  end
end
