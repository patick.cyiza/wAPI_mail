class AddUserPublicTemplate < ActiveRecord::Migration[5.1]
  def change
    add_reference :public_templates, :user, index: true
  end
end
