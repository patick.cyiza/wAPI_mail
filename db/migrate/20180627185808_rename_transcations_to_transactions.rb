class RenameTranscationsToTransactions < ActiveRecord::Migration[5.1]
  def self.up
    rename_table :transcations, :transactions
  end

  def self.down
    rename_table :transactions, :transcations
  end
end
