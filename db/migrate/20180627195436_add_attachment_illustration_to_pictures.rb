class AddAttachmentIllustrationToPictures < ActiveRecord::Migration[5.1]
  def self.up
    change_table :pictures do |t|
      t.attachment :illustration
    end
  end

  def self.down
    remove_attachment :pictures, :illustration
  end
end
