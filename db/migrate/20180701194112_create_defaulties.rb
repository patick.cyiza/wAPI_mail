class CreateDefaulties < ActiveRecord::Migration[5.1]
  def change
    create_table :defaulties do |t|
      t.references :project, foreign_key: true
      t.references :defaultable, polymorphic: true

      t.timestamps
    end
  end
end
