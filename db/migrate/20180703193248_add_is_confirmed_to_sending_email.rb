class AddIsConfirmedToSendingEmail < ActiveRecord::Migration[5.1]
  def change
    add_column :sending_emails, :confirmed_at, :datetime
  end
end
