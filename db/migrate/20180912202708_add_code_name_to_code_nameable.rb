class AddCodeNameToCodeNameable < ActiveRecord::Migration[5.1]
  def change
    add_column :transactions, :code_name, :string
    add_column :projects, :code_name, :string
  end
end
