class AddSubjectToTransaction < ActiveRecord::Migration[5.1]
  def change
    add_column :transactions, :subject, :string
  end
end
