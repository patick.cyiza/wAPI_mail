class ChangeNameCodeToApiId < ActiveRecord::Migration[5.1]
  def change
    rename_column :projects, :code_name, :api_id
  end
end
