class CreateSendApiKeys < ActiveRecord::Migration[5.1]
  def change
    create_table :send_api_keys do |t|
      t.references :project, foreign_key: true
      t.text :public_key
      t.string :sender_id
      t.boolean :for_production

      t.timestamps
    end
  end
end
