class RenameViriableTable < ActiveRecord::Migration[5.2]
  def self.up
    rename_table :editables_variables, :designables_variables
  end

  def self.down
    rename_table :designables_variables, :editables_variables
  end
end
