class ReworkDesignablesVariable < ActiveRecord::Migration[5.2]
  def change
    remove_column :designables_variables, :sending_variable_id
  end
end
