class CreateSendableAssets < ActiveRecord::Migration[5.2]
  def change
    create_table :sendable_assets do |t|
      t.references :attachable, polymorphic: true
      t.string :code_name
      t.string :asset_type

      t.timestamps
    end
  end
end
