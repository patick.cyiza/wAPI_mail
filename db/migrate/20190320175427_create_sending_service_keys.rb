class CreateSendingServiceKeys < ActiveRecord::Migration[5.2]
  def change
    create_table :sending_service_keys do |t|
      t.references :sending_service, foreign_key: true
      t.string :key_name
      t.text :key_value

      t.timestamps
    end
  end
end
