class DeleteKeyValues < ActiveRecord::Migration[5.2]
  def change
    remove_column :sending_service_keys, :key_value
  end
end
