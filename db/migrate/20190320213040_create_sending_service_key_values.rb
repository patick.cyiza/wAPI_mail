class CreateSendingServiceKeyValues < ActiveRecord::Migration[5.2]
  def change
    create_table :sending_service_key_values do |t|
      t.references :sending_service_key
      t.references :project, foreign_key: true
      t.text :key_value

      t.timestamps
    end
  end
end
