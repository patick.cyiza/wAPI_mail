class ChangeKeyvalueProjectReferences < ActiveRecord::Migration[5.2]
  def change
    remove_reference(:sending_service_key_values, :project, foreign_key: true)
    add_reference(:sending_service_key_values, :sending_services_project, foreign_key: true)
    remove_reference(:sending_service_key_values, :sending_service_key)
    add_reference(:sending_service_key_values, :sending_service_key, foreign_key: true)
  end
end
