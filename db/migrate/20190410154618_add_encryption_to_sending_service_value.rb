class AddEncryptionToSendingServiceValue < ActiveRecord::Migration[5.2]
  def change
    rename_column :sending_service_key_values, :key_value, :encrypted_value
    add_column :sending_service_key_values, :encrypted_value_iv, :string, unique: true
  end
end
