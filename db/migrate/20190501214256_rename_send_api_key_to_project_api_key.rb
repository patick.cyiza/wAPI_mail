class RenameSendApiKeyToProjectApiKey < ActiveRecord::Migration[5.2]
  def self.up
    rename_table :send_api_keys, :project_api_keys
  end

  def self.down
    rename_table :project_api_keys, :send_api_keys
  end
end
