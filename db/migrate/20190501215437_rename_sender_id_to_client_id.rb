class RenameSenderIdToClientId < ActiveRecord::Migration[5.2]
  def change
    rename_column :project_api_keys, :sender_id, :client_id
  end
end
