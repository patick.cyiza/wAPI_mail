class RenameSendingServiceToSendingProvider < ActiveRecord::Migration[5.2]
  def self.up
    rename_table :sending_services, :sending_providers
  end

  def self.down
    rename_table :sending_providers, :sending_services
  end
end
