class RenameSendingServiceKeyToSendingProviderKey < ActiveRecord::Migration[5.2]
  def self.up
    rename_table :sending_service_keys, :sending_provider_keys
  end

  def self.down
    rename_table :sending_provider_keys, :sending_service_keys
  end
end
