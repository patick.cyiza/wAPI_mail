class RenameSendingServiceKeyValueToSendingProviderKeyValue < ActiveRecord::Migration[5.2]
  def self.up
    rename_table :sending_service_key_values, :provider_key_values
  end

  def self.down
    rename_table :provider_key_values, :sending_service_key_values
  end
end
