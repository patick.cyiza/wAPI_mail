class RenameSendingServicesProjectToSendingProvidersProject < ActiveRecord::Migration[5.2]
  def self.up
    rename_table :sending_services_projects, :sending_providers_projects
  end

  def self.down
    rename_table :sending_providers_projects, :sending_services_projects
  end
end
