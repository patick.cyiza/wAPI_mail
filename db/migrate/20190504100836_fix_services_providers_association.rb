class FixServicesProvidersAssociation < ActiveRecord::Migration[5.2]
  def change
    rename_column :sending_provider_keys, :sending_service_id, :sending_provider_id
    rename_column :sending_providers_projects, :sending_service_id, :sending_provider_id
    rename_column :provider_key_values, :sending_service_key_id, :sending_provider_key_id
    rename_column :provider_key_values, :sending_services_project_id, :sending_providers_project_id
  end
end
