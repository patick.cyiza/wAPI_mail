# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_05_04_100836) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string "namespace"
    t.text "body"
    t.string "resource_type"
    t.bigint "resource_id"
    t.string "author_type"
    t.bigint "author_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace"
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"
  end

  create_table "admin_users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admin_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true
  end

  create_table "defaulties", force: :cascade do |t|
    t.bigint "project_id"
    t.string "defaultable_type"
    t.bigint "defaultable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["defaultable_type", "defaultable_id"], name: "index_defaulties_on_defaultable_type_and_defaultable_id"
    t.index ["project_id"], name: "index_defaulties_on_project_id"
  end

  create_table "designables_variables", force: :cascade do |t|
    t.string "editable_type"
    t.bigint "editable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["editable_type", "editable_id"], name: "index_designables_variables_on_editable_type_and_editable_id"
  end

  create_table "mail_bodies", force: :cascade do |t|
    t.bigint "user_id"
    t.string "name"
    t.text "content_img"
    t.text "styling_img"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_mail_bodies_on_user_id"
  end

  create_table "pictures", force: :cascade do |t|
    t.string "illustrable_type"
    t.bigint "illustrable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "illustration_file_name"
    t.string "illustration_content_type"
    t.integer "illustration_file_size"
    t.datetime "illustration_updated_at"
    t.index ["illustrable_type", "illustrable_id"], name: "index_pictures_on_illustrable_type_and_illustrable_id"
  end

  create_table "project_api_keys", force: :cascade do |t|
    t.bigint "project_id"
    t.text "public_key"
    t.string "client_id"
    t.boolean "for_production"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["project_id"], name: "index_project_api_keys_on_project_id"
  end

  create_table "projects", force: :cascade do |t|
    t.bigint "user_id"
    t.string "name"
    t.text "descrpition"
    t.string "url_to_project"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "api_id"
    t.index ["user_id"], name: "index_projects_on_user_id"
  end

  create_table "provider_key_values", force: :cascade do |t|
    t.text "encrypted_value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "sending_providers_project_id"
    t.bigint "sending_provider_key_id"
    t.string "encrypted_value_iv"
    t.index ["sending_provider_key_id"], name: "index_provider_key_values_on_sending_provider_key_id"
    t.index ["sending_providers_project_id"], name: "index_provider_key_values_on_sending_providers_project_id"
  end

  create_table "public_templates", force: :cascade do |t|
    t.text "content_img"
    t.text "styling_img"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.index ["user_id"], name: "index_public_templates_on_user_id"
  end

  create_table "sendable_assets", force: :cascade do |t|
    t.string "attachable_type"
    t.bigint "attachable_id"
    t.string "code_name"
    t.string "asset_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["attachable_type", "attachable_id"], name: "index_sendable_assets_on_attachable_type_and_attachable_id"
  end

  create_table "sending_emails", force: :cascade do |t|
    t.bigint "project_id"
    t.string "email"
    t.boolean "is_default"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "confirmed_at"
    t.index ["project_id"], name: "index_sending_emails_on_project_id"
  end

  create_table "sending_provider_keys", force: :cascade do |t|
    t.bigint "sending_provider_id"
    t.string "key_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["sending_provider_id"], name: "index_sending_provider_keys_on_sending_provider_id"
  end

  create_table "sending_providers", force: :cascade do |t|
    t.string "name"
    t.string "sending_controller_url"
    t.string "api_access_docs_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "build_state"
  end

  create_table "sending_providers_projects", force: :cascade do |t|
    t.bigint "sending_provider_id"
    t.bigint "project_id"
    t.boolean "is_default"
    t.string "secret_key"
    t.string "public_key"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["project_id"], name: "index_sending_providers_projects_on_project_id"
    t.index ["sending_provider_id"], name: "index_sending_providers_projects_on_sending_provider_id"
  end

  create_table "sending_variables", force: :cascade do |t|
    t.bigint "user_id"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_sending_variables_on_user_id"
  end

  create_table "templates", force: :cascade do |t|
    t.bigint "project_id"
    t.string "name"
    t.text "content_img"
    t.text "styling_img"
    t.boolean "is_default"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["project_id"], name: "index_templates_on_project_id"
  end

  create_table "transactions", force: :cascade do |t|
    t.bigint "project_id"
    t.bigint "sending_email_id"
    t.bigint "template_id"
    t.bigint "mail_body_id"
    t.string "name"
    t.text "content_img"
    t.text "styling_img"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "code_name"
    t.string "subject"
    t.index ["mail_body_id"], name: "index_transactions_on_mail_body_id"
    t.index ["project_id"], name: "index_transactions_on_project_id"
    t.index ["sending_email_id"], name: "index_transactions_on_sending_email_id"
    t.index ["template_id"], name: "index_transactions_on_template_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.boolean "is_admin"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "defaulties", "projects"
  add_foreign_key "mail_bodies", "users"
  add_foreign_key "project_api_keys", "projects"
  add_foreign_key "projects", "users"
  add_foreign_key "provider_key_values", "sending_provider_keys"
  add_foreign_key "provider_key_values", "sending_providers_projects"
  add_foreign_key "sending_emails", "projects"
  add_foreign_key "sending_provider_keys", "sending_providers"
  add_foreign_key "sending_providers_projects", "projects"
  add_foreign_key "sending_providers_projects", "sending_providers"
  add_foreign_key "sending_variables", "users"
  add_foreign_key "templates", "projects"
  add_foreign_key "transactions", "mail_bodies"
  add_foreign_key "transactions", "projects"
  add_foreign_key "transactions", "sending_emails"
  add_foreign_key "transactions", "templates"
end
