# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
# AdminUser.create!(email: Rails.application.secrets.admin_email, password: Rails.application.secrets.admin_password, password_confirmation: Rails.application.secrets.admin_password)

SendingProvider.create!([
  {:name=>"mailjet", :sending_controller_url=>"https://github.com/mailjet/mailjet-gem", :api_access_docs_url=>"https://www.mailjet.com/support/how-to-create-a-sub-account-or-additional-api-key,738", :build_state=>"supported"},
  {:name=>"sendgrid", :sending_controller_url=>"https://github.com/sendgrid/sendgrid-ruby", :api_access_docs_url=>"https://sendgrid.com/docs/ui/account-and-settings/api-keys/", :build_state=>"supported"},
  {:name=>"mailgun", :sending_controller_url=>"https://github.com/mailgun/mailgun-ruby/blob/master/docs/Messages.md", :api_access_docs_url=>"https://help.mailgun.com/hc/en-us/articles/203380100-Where-Can-I-Find-My-API-Key-and-SMTP-Credentials-", :build_state=>"supported"},
  {:name=>"mandrill", :sending_controller_url=>"https://mandrillapp.com/api/docs/index.ruby.html", :api_access_docs_url=>"https://www.inboundnow.com/how-to-get-your-mandrill-api-key", :build_state=>"supported"},
  {:name=>"sprakpost", :sending_controller_url=>"https://developers.sparkpost.com/api/transmissions/", :api_access_docs_url=>"https://www.sparkpost.com/docs/getting-started/create-api-keys/", :build_state=>"supported"},
])

SendingProviderKey.create!([
   {:sending_provider_id=>1, :key_name=>"api_key"},
   {:sending_provider_id=>1, :key_name=>"secret_key"},
   {:sending_provider_id=>2, :key_name=>"api_key"},
   {:sending_provider_id=>3, :key_name=>"sending_domain"},
   {:sending_provider_id=>3, :key_name=>"api_key"},
   {:sending_provider_id=>4, :key_name=>"api_key"},
   {:sending_provider_id=>5, :key_name=>"api_key"}
])
