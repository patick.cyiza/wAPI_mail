FactoryBot.define do
  factory :editables_variable do
    sending_variable nil
    editable nil
  end
end
