FactoryBot.define do
  factory :ProjectApiKey do
    project nil
    public_key "MyText"
    sender_id "MyString"
    for_production false
  end
end
