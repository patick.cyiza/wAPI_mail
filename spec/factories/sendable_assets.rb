FactoryBot.define do
  factory :sendable_asset do
    attachable { nil }
    code_name { "MyString" }
  end
end
