FactoryBot.define do
  factory :sending_email do
    project nil
    email "MyString"
    is_default false
  end
end
