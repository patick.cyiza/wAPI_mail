FactoryBot.define do
  factory :ProviderKeyValue do
    sending_service_key { "" }
    project { nil }
    key_value { "MyText" }
  end
end
