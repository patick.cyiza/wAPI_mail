FactoryBot.define do
  factory :SendingProviderKey do
    sending_service { nil }
    key_name { "MyString" }
    key_value { "MyText" }
  end
end
