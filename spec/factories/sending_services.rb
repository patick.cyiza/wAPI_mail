FactoryBot.define do
  factory :SendingProvider do
    name "MyString"
    sending_controller_url "MyString"
    api_access_docs_url "MyString"
  end
end
