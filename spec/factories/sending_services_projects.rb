FactoryBot.define do
  factory :SendingProvidersProject do
    sending_service nil
    project nil
    is_default false
    secret_key "MyString"
    public_key "MyString"
  end
end
