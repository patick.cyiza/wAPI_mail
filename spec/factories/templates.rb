FactoryBot.define do
  factory :template do
    project nil
    name "MyString"
    content_img "MyText"
    styling_img "MyText"
    is_default false
  end
end
