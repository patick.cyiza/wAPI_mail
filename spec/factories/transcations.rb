FactoryBot.define do
  factory :transcation do
    project nil
    sending_email nil
    template nil
    mail_body nil
    name "MyString"
    content_img "MyText"
    styling_img "MyText"
  end
end
