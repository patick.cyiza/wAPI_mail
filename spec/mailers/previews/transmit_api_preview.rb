# Preview all emails at http://localhost:3000/rails/mailers/transmit_api
class TransmitApiPreview < ActionMailer::Preview
  def check_result
    content_hash = JwtService.params_unzipper params[:zipped_params]

    TransmitApiMailer.prepare_for_preview content_hash, {from: content_hash[:from], to: nil}
  end
end
